package io.agora.agorachat;

import android.app.Application;

import io.agora.AgoraAPIOnlySignal;
import io.agora.agorachat.media.WorkerThread;
import io.agora.agoramessagetubekit.AgoraMessageTubeKit;

public class MyApplication extends Application {

    private WorkerThread mWorkerThread;

    private AgoraMessageTubeKit mAgoraMessageTubeKit;

    private static MyApplication mInstance;

    public MyApplication() {
        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initSignalInstance();
    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    public synchronized WorkerThread getWorkerThread() {
        return mWorkerThread;
    }

    public synchronized void initWorkerThread() {
        if (mWorkerThread == null) {
            mWorkerThread = new WorkerThread(getApplicationContext());
            mWorkerThread.start();

            mWorkerThread.waitForReady();
        }
    }

    public synchronized void deInitWorkerThread() {
        mWorkerThread.exit();
        try {
            mWorkerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mWorkerThread = null;
    }

    private void initSignalInstance() {
        mAgoraMessageTubeKit = new AgoraMessageTubeKit(this, getString(R.string.private_app_id));
    }

    public AgoraMessageTubeKit getSignalInstance() {
        return mAgoraMessageTubeKit;
    }
}
