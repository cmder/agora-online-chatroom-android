package io.agora.agorachat.activity;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

import io.agora.agorachat.MyApplication;
import io.agora.agorachat.constants.AppConstants;
import io.agora.agorachat.media.EngineConfig;
import io.agora.agorachat.media.EngineEventHandlerManager;
import io.agora.agorachat.media.WorkerThread;
import io.agora.agorachat.utils.StatusBarUtils;
import io.agora.rtc.RtcEngine;

public abstract class BaseActivity extends Activity {
    private final static Logger log = LoggerFactory.getLogger(BaseActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActionBar() != null) {
            getActionBar().hide();
        }

        // 设置全屏
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        StatusBarUtils.setTransparent(this);

        // 设置 fitsSystemWindows 属性
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            findViewById(R.id.ll_chat_room_bg).setFitsSystemWindows(true);
        }*/

        // 让布局向上移来显示软键盘
        // 不自动弹出键盘
        // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        final View layout = findViewById(Window.ID_ANDROID_CONTENT);
        ViewTreeObserver vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                initUIAndEvent();
            }
        });
    }

    protected abstract void initUIAndEvent();

    protected abstract void deInitUIAndEvent();

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isFinishing()) {
                    return;
                }

                if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)) {
                    boolean checkPermissionResult = checkSelfPermissions();
                }
            }
        }, 500);
    }

    private boolean checkSelfPermissions() {
        return checkSelfPermission(Manifest.permission.RECORD_AUDIO, AppConstants.PERMISSION_REQ_ID_RECORD_AUDIO) &&
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, AppConstants.PERMISSION_REQ_ID_WRITE_EXTERNAL_STORAGE);
    }

    @Override
    protected void onDestroy() {
        deInitUIAndEvent();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        log.debug("onRequestPermissionsResult " + requestCode + " " + Arrays.toString(permissions) + " " + Arrays.toString(grantResults));
        switch (requestCode) {
            case AppConstants.PERMISSION_REQ_ID_RECORD_AUDIO: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, AppConstants.PERMISSION_REQ_ID_WRITE_EXTERNAL_STORAGE);
                    ((MyApplication) getApplication()).initWorkerThread();
                } else {
                    finish();
                }
                break;
            }
            case AppConstants.PERMISSION_REQ_ID_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    finish();
                }
                break;
            }
        }
    }

    public boolean checkSelfPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            return false;
        }
        if (Manifest.permission.RECORD_AUDIO.equals(permission)) {
            ((MyApplication) getApplication()).initWorkerThread();
        }
        return true;
    }

    protected RtcEngine getRtcEngine() {
        return ((MyApplication) getApplication()).getWorkerThread().getRtcEngine();
    }

    protected final WorkerThread getWorkerThread() {
        return ((MyApplication) getApplication()).getWorkerThread();
    }

    protected final EngineConfig getEngineConfig() {
        return ((MyApplication) getApplication()).getWorkerThread().getEngineConfig();
    }

    protected final EngineEventHandlerManager getEngineEventHandleManager() {
        return ((MyApplication) getApplication()).getWorkerThread().getEngineEventHandlerManager();
    }
}
