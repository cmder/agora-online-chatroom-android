package io.agora.agorachat.activity;

import android.app.AlertDialog;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.agora.agorachat.MyApplication;
import io.agora.agorachat.R;
import io.agora.agorachat.adapter.ChangeVoiceRecyclerAdapter;
import io.agora.agorachat.adapter.MsgListRecyclerAdapter;
import io.agora.agorachat.adapter.MusicSelectorRecyclerAdapter;
import io.agora.agorachat.adapter.RoomBgRecyclerAdapter;
import io.agora.agorachat.adapter.UsrListRecyclerAdapter;
import io.agora.agorachat.bean.BgMusicItem;
import io.agora.agorachat.bean.ChangeVoiceItem;
import io.agora.agorachat.bean.Msg;
import io.agora.agorachat.bean.MsgListItem;
import io.agora.agorachat.bean.RoomBgItem;
import io.agora.agorachat.bean.Usr;
import io.agora.agorachat.constants.AppConstants;
import io.agora.agorachat.constants.MsgType;
import io.agora.agorachat.media.SubEngineEventHandler;
import io.agora.agorachat.utils.BitmapUtils;
import io.agora.agoramessagetubekit.AbstractMTKCallback;
import io.agora.agoramessagetubekit.AgoraMessageTubeKit;

import static io.agora.rtc.Constants.AUDIO_PROFILE_DEFAULT;
import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_HIGH_QUALITY_STEREO;
import static io.agora.rtc.Constants.AUDIO_SCENARIO_DEFAULT;
import static io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE;
import static io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER;

public class ChatRoomActivity extends BaseActivity implements SubEngineEventHandler {

    @BindView(R.id.chat_room_txt_topic)
    TextView chatRoomTxtTopic;
    @BindView(R.id.chat_room_txt_num)
    TextView chatRoomTxtNum;
    @BindView(R.id.chat_room_img_config)
    ImageView chatRoomImgConfig;
    @BindView(R.id.set_room_bg_rl_top)
    RelativeLayout setRoomBgRlTop;
    @BindView(R.id.chat_room_img_portrait)
    CircleImageView chatRoomImgPortrait;
    @BindView(R.id.chat_room_txt_host_name)
    TextView chatRoomTxtHostName;
    @BindView(R.id.chat_room_recycler_participants)
    RecyclerView chatRoomRecyclerParticipants;
    @BindView(R.id.chat_room_recycler_msg_list)
    RecyclerView chatRoomRecyclerMsgList;
    @BindView(R.id.chat_room_edt_msg)
    EditText chatRoomEdtMsg;
    @BindView(R.id.chat_room_speak)
    ImageView chatRoomSpeak;
    @BindView(R.id.chat_room_sound)
    ImageView chatRoomSound;
    @BindView(R.id.chat_room_ll_msg)
    RelativeLayout chatRoomLlMsg;
    @BindView(R.id.ll_chat_room_bg)
    RelativeLayout llChatRoomBg;
    @BindView(R.id.chat_room_img_back)
    ImageView chatRoomImgBack;

    private LayoutInflater mInflater;

    private int mRole = -1;
    private String mAccount = null; // 用户名

    private UsrListRecyclerAdapter mUsrListRecyclerAdapter;
    private List<Usr> mUsrListItems;

    private MsgListRecyclerAdapter mMsgListRecyclerAdapter;
    private List<MsgListItem> mMsgListItems;

    private Gson mGson;

    private String mChannelName;

    private AgoraMessageTubeKit mSignalInstance;

    private int[] paths = {R.mipmap.portrait_0, R.mipmap.portrait_1, R.mipmap.portrait_2, R.mipmap.portrait_3};


    private List<RoomBgItem> roomBgItems; // 房间背景数据
    private List<BgMusicItem> bgMusicItems; // 背景音乐数据
    private List<ChangeVoiceItem> changeVoiceItems; // 变声数据

    private int mSelectedRoomBgPosition;
    private int mSelectedChangeVoicePosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        ButterKnife.bind(this);

        mInflater = LayoutInflater.from(this);

        mGson = new Gson();

        mChannelName = getIntent().getStringExtra(AppConstants.ACTION_KEY_CHANNEL_NAME);

        mSignalInstance = MyApplication.getInstance().getSignalInstance();
        addCallbackForSignal();

        initData();
    }

    private void initData() {
        roomBgItems = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            if (i == 0) {
                roomBgItems.add(new RoomBgItem(true));
            } else {
                roomBgItems.add(new RoomBgItem(false));
            }
        }

        bgMusicItems = new ArrayList<>();
        String[] names = {"爱情", "纯粹", "爱", "纯真", "彩虹", "彩云之巅", "灿烂", "晨星", "像风一样自由"};
        String[] sizes = {"4.90M", "4.73M", "4.70M", "4.12M", "4.04M", "3.97M", "3.89M", "3.62M", "3.37M"};
        for (int i = 0; i < 9; i++) {
            BgMusicItem item = new BgMusicItem(names[i], sizes[i], "许巍", "/assets/music_" + i + ".mp3", 1);
            bgMusicItems.add(item);
        }

        changeVoiceItems = new ArrayList<>();
        String[] titles = new String[]{"老人", "萝莉", "正太", "猪八戒", "绿巨人", "空灵"};
        for (int i = 0; i < titles.length; i++) {
            ChangeVoiceItem item = new ChangeVoiceItem(titles[i], false);
            changeVoiceItems.add(item);
        }
    }

    @Override
    protected void initUIAndEvent() {
        getEngineEventHandleManager().addEventHandler(this);

        // 初始化聊天话题
        if (TextUtils.isEmpty(getIntent().getStringExtra(AppConstants.ACTION_KEY_CHAT_TOPIC))) {
            chatRoomTxtTopic.setText("未命名");
        } else {
            chatRoomTxtTopic.setText(getIntent().getStringExtra(AppConstants.ACTION_KEY_CHAT_TOPIC));
        }

        // 初始化头像
        chatRoomImgPortrait.setImageBitmap(BitmapUtils.readBitMap(this, paths[new Random().nextInt(3)]));

        // 初始化头像列表
        mUsrListItems = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            mUsrListItems.add(new Usr());
        }
        mUsrListRecyclerAdapter = new UsrListRecyclerAdapter(this, mUsrListItems);
        GridLayoutManager usrLayoutManager = new GridLayoutManager(this, 4);
        chatRoomRecyclerParticipants.setLayoutManager(usrLayoutManager);
        chatRoomRecyclerParticipants.setAdapter(mUsrListRecyclerAdapter);

        // 初始化人数
        chatRoomTxtNum.setText(mUsrListItems.size() + "人");

        // 初始化消息列表
        mMsgListItems = new ArrayList<>();
        mMsgListRecyclerAdapter = new MsgListRecyclerAdapter(this, mMsgListItems);
        LinearLayoutManager msgLayoutManager = new LinearLayoutManager(this);
        chatRoomRecyclerMsgList.setLayoutManager(msgLayoutManager);
        chatRoomRecyclerMsgList.setAdapter(mMsgListRecyclerAdapter);

        mRole = getIntent().getIntExtra(AppConstants.ACTION_KEY_CLIENT_ROLE, CLIENT_ROLE_BROADCASTER);
        if (mRole == CLIENT_ROLE_AUDIENCE) {
            getWorkerThread().getRtcEngine().setAudioProfile(AUDIO_PROFILE_DEFAULT, AUDIO_SCENARIO_DEFAULT);
        } else {
            getWorkerThread().getRtcEngine().setAudioProfile(AUDIO_PROFILE_MUSIC_HIGH_QUALITY_STEREO, AUDIO_SCENARIO_DEFAULT);
        }

        getWorkerThread().getRtcEngine().setClientRole(mRole != CLIENT_ROLE_AUDIENCE ? CLIENT_ROLE_BROADCASTER : CLIENT_ROLE_AUDIENCE);

        getWorkerThread().joinChannel(mChannelName, getEngineConfig().getUid());

        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
    }

    @Override
    protected void deInitUIAndEvent() {
        getWorkerThread().leaveChannel(getEngineConfig().getChannel());
        getEngineEventHandleManager().removeEventHandler(this);

        // 发送离开频道的频道消息
        mSignalInstance.sendChannelMessage(mGson.toJson(new Msg(mRole, MsgType.MSG_TYPE_LEAVE_CHANNEL, mAccount, "离开频道")));

        mSignalInstance.unregisterCallback();
        mSignalInstance.leaveChannel();
    }

    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        // 登录信令
        mAccount = "" + (uid & 0xFFFFFFFFL);
        mSignalInstance.joinChannel(channel, mAccount);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatRoomTxtHostName.setText(mAccount);
            }
        });
    }

    private void addCallbackForSignal() {
        mSignalInstance.registerCallback(new AbstractMTKCallback() {
            @Override
            public void onChannelJoined(String channelID) {
                super.onChannelJoined(channelID);
            }

            @Override
            public void onChannelJoinFailed(String channelID, int ecode) {
                super.onChannelJoinFailed(channelID, ecode);
            }

            @Override
            public void onChannelLeaved(String channelID, int ecode) {
                super.onChannelLeaved(channelID, ecode);
            }

            @Override
            public void onChannelUserJoined(String account, int uid) {
                super.onChannelUserJoined(account, uid);
            }

            @Override
            public void onChannelUserLeaved(String account, int uid) {
                super.onChannelUserLeaved(account, uid);
            }

            @Override
            public void onMessageInstantReceive(String account, int uid, String msg) {
                super.onMessageInstantReceive(account, uid, msg);
            }

            @Override
            public void onMarkedMessageInstantReceive(String account, int uid, String msg) {
                super.onMarkedMessageInstantReceive(account, uid, msg);
            }

            @Override
            public void onMessageChannelReceive(String channelID, String account, int uid, String msg) {
                super.onMessageChannelReceive(channelID, account, uid, msg);
            }

            @Override
            public void onMarkedMessageChannelReceive(String channelID, String account, int uid, String msg) {
                super.onMarkedMessageChannelReceive(channelID, account, uid, msg);
            }

            @Override
            public void onMessageSendSuccess(String messageID) {
                super.onMessageSendSuccess(messageID);
            }

            @Override
            public void onMessageSendError(String messageID, int ecode) {
                super.onMessageSendError(messageID, ecode);
            }
        });
    }

    @OnClick(R.id.chat_room_img_config)
    public void onChatRoomImgConfigClicked() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.setContentView(R.layout.dialog_config_room);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // 5.0 全透明实现
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else { // 4.4 全透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        // 设置房间背景
        RelativeLayout imgRoomBg = alertDialog.findViewById(R.id.config_room_bg_point);
        imgRoomBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                alertSetRoomBgDialog();
            }
        });
        // 设置背景音乐
        RelativeLayout imgBgMusic = alertDialog.findViewById(R.id.config_room_bg_music_point);
        imgBgMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                alertSetBgMusicDialog();
            }
        });
        // 设置变声
        RelativeLayout imgChangeVoice = alertDialog.findViewById(R.id.config_room_change_voice_point);
        imgChangeVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                alertChangeVoiceDialog();
            }
        });
        // 退出房间
        Button btnQuit = alertDialog.findViewById(R.id.config_room_btn_quit);
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatRoomActivity.this.finish();
            }
        });

        WindowManager manager = this.getWindowManager();
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        int width = outMetrics.widthPixels;
        int height = outMetrics.heightPixels;
        window.setLayout(width * 818 / 1080, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.RIGHT);
    }

    /**
     * 设置房间背景
     */
    private void alertSetRoomBgDialog() {
        final int lastSelectedRoomBgPosition = mSelectedRoomBgPosition;

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setContentView(R.layout.dialog_set_room_bg);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // 5.0 全透明实现
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else { // 4.4 全透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        // 初始化房间背景选项
        RecyclerView roomBgRecycler = alertDialog.findViewById(R.id.set_room_bg_recycler);
        final RoomBgRecyclerAdapter roomBgRecyclerAdapter = new RoomBgRecyclerAdapter(this, roomBgItems);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        roomBgRecycler.setLayoutManager(gridLayoutManager);
        roomBgRecycler.setAdapter(roomBgRecyclerAdapter);
        roomBgRecyclerAdapter.setOnItemClickListener(new RoomBgRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                mSelectedRoomBgPosition = position;
                roomBgRecyclerAdapter.setChosen(position);
            }
        });

        // 返回
        ImageView imgBack = alertDialog.findViewById(R.id.set_room_bg_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedRoomBgPosition = lastSelectedRoomBgPosition;
                roomBgRecyclerAdapter.setChosen(mSelectedRoomBgPosition);
                alertDialog.dismiss();
            }
        });

        // 确定
        Button btnConfirm = alertDialog.findViewById(R.id.set_room_bg_btn_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = getResources().getIdentifier("io.agora.agorachat:mipmap/bg_room_" + mSelectedRoomBgPosition, null, null);
                llChatRoomBg.setBackgroundResource(id);

                // TODO: 2018/7/12 发送修改背景的消息
                // mSignal.messageChannelSend(mChannelName, mGson.toJson(new Msg(mRole, MsgType.MSG_TYPE_CHANGE_IMG, mAccount, "" + position)), null);

                alertDialog.dismiss();
            }
        });

        // 取消
        Button btnCancel = alertDialog.findViewById(R.id.set_room_bg_btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedRoomBgPosition = lastSelectedRoomBgPosition;
                roomBgRecyclerAdapter.setChosen(mSelectedRoomBgPosition);
                alertDialog.dismiss();
            }
        });


    }

    /**
     * 设置背景音乐
     */
    private void alertSetBgMusicDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setContentView(R.layout.dialog_set_bg_music);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // 5.0 全透明实现
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else { // 4.4 全透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        // 返回
        ImageView imgBack = alertDialog.findViewById(R.id.set_bg_music__back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        // 初始化音乐列表
        RecyclerView bgMusicRecycler = alertDialog.findViewById(R.id.set_bg_music_recycler);
        final MusicSelectorRecyclerAdapter musicSelectorRecyclerAdapter = new MusicSelectorRecyclerAdapter(this, bgMusicItems);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        bgMusicRecycler.setLayoutManager(linearLayoutManager);
        bgMusicRecycler.setAdapter(musicSelectorRecyclerAdapter);
        musicSelectorRecyclerAdapter.setOnItemClickListener(new MusicSelectorRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                if (bgMusicItems.get(position).getState() == 0) {
                    // 暂停
                    bgMusicItems.get(position).setState(1);
                    musicSelectorRecyclerAdapter.notifyItemChanged(position);
                    getWorkerThread().getRtcEngine().stopAudioMixing();
                } else if (bgMusicItems.get(position).getState() == 1) {
                    // 播放音乐
                    for (int i = 0; i < bgMusicItems.size(); i++) {
                        if (i == position) {
                            bgMusicItems.get(i).setState(0);
                        } else {
                            bgMusicItems.get(i).setState(1);
                        }
                    }
                    musicSelectorRecyclerAdapter.notifyDataSetChanged();
                    getWorkerThread().getRtcEngine().startAudioMixing(bgMusicItems.get(position).getPath(), false, false, -1);
                }
            }
        });
    }

    /**
     * 变声
     */
    private void alertChangeVoiceDialog() {
        final int lastSelectedChangeVoicePosition = mSelectedChangeVoicePosition;

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setContentView(R.layout.dialog_change_voice);
        // status bar 的透明实现
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // 5.0 全透明实现
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else { // 4.4 全透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        // 初始化选项列表
        final RecyclerView options = alertDialog.findViewById(R.id.change_voice_recycler_options);
        final ChangeVoiceRecyclerAdapter adapter = new ChangeVoiceRecyclerAdapter(this, changeVoiceItems);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        options.setLayoutManager(layoutManager);
        options.setAdapter(adapter);
        adapter.setOnItemClickListener(new ChangeVoiceRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                if (mSelectedChangeVoicePosition == position) {
                    mSelectedChangeVoicePosition = -1;
                } else {
                    mSelectedChangeVoicePosition = position;
                }
                adapter.setChosen(mSelectedChangeVoicePosition);
            }
        });
        // 返回
        ImageView imgBack = alertDialog.findViewById(R.id.change_voice_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedChangeVoicePosition = lastSelectedChangeVoicePosition;
                adapter.setChosen(mSelectedChangeVoicePosition);
                alertDialog.dismiss();
            }
        });
        // 确定
        Button btnConfirm = alertDialog.findViewById(R.id.change_voice_btn_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2018/7/12 变声
                alertDialog.dismiss();
            }
        });
        // 取消
        Button btnCancel = alertDialog.findViewById(R.id.change_voice_btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedChangeVoicePosition = lastSelectedChangeVoicePosition;
                adapter.setChosen(mSelectedChangeVoicePosition);
                alertDialog.dismiss();
            }
        });
    }

    @OnClick(R.id.chat_room_speak)
    public void onChatRoomSpeakClicked() {

    }

    @OnClick(R.id.chat_room_sound)
    public void onChatRoomSoundClicked() {

    }

    @OnClick(R.id.chat_room_img_back)
    public void onChatRoomImgBackClicked() {
        this.finish();
    }
}
