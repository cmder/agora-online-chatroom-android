package io.agora.agorachat.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.agora.agorachat.R;
import io.agora.agorachat.adapter.ChatRoomRecyclerAdapter;
import io.agora.agorachat.constants.AppConstants;
import io.agora.agorachat.constants.TopicConstants;
import io.agora.agorachat.utils.DimensionsUtils;

public class MainActivity extends BaseActivity {

    @BindView(R.id.recycler_rooms)
    RecyclerView recyclerRooms;
    @BindView(R.id.btn_create_room)
    Button btnCreateRoom;
    @BindView(R.id.btn_join_room)
    Button btnJoinRoom;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private LayoutInflater mInflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mInflater = this.getLayoutInflater();

        recyclerRooms.setHasFixedSize(true);

        mLayoutManager = new GridLayoutManager(this, 2);
        recyclerRooms.setLayoutManager(mLayoutManager);

        mAdapter = new ChatRoomRecyclerAdapter(this);
        recyclerRooms.setAdapter(mAdapter);
    }

    @Override
    protected void initUIAndEvent() {

    }

    @Override
    protected void deInitUIAndEvent() {

    }

    @OnClick(R.id.btn_create_room)
    public void onBtnCreateRoomClicked() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.setContentView(R.layout.dialog_create_room);
        // 这两句话是为了弹出软键盘
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        // 让对话框显示在屏幕底部
        WindowManager manager = this.getWindowManager();
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        int width = outMetrics.widthPixels;
        int height = outMetrics.heightPixels;
        window.setLayout(width, (int) DimensionsUtils.dp2px(this, 295.33f));
        window.setGravity(Gravity.BOTTOM);

        final EditText createRoomEdtTopic = alertDialog.findViewById(R.id.create_room_edt_topic);
        Button createRoomBtnRandomTopic = alertDialog.findViewById(R.id.create_room_btn_random_topic);
        final EditText createRoomEdtChannelName = alertDialog.findViewById(R.id.create_room_edt_channel_name);
        TextView createRoomTxtMode0 = alertDialog.findViewById(R.id.create_room_txt_mode_0);
        TextView createRoomTxtMode1 = alertDialog.findViewById(R.id.create_room_txt_mode_1);
        TextView createRoomTxtMode2 = alertDialog.findViewById(R.id.create_room_txt_mode_2);
        TextView createRoomTxtMode3 = alertDialog.findViewById(R.id.create_room_txt_mode_3);
        Button btnCreateRoom = alertDialog.findViewById(R.id.btn_create_room);
        createRoomBtnRandomTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createRoomEdtTopic.setText(TopicConstants.getRondomTopic());
            }
        });
        final TextView[] modes = new TextView[]{createRoomTxtMode0, createRoomTxtMode1, createRoomTxtMode2, createRoomTxtMode3};
        for (int i = 0; i < modes.length; i++) {
            final int finalI = i;
            modes[finalI].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    modes[finalI].setTextColor(Color.parseColor("#09BDF4"));
                    modes[finalI].setBackground(getResources().getDrawable(R.drawable.bg_txt_selected));
                    for (int j = 0; j < modes.length; j++) {
                        if (j != finalI) {
                            modes[j].setTextColor(Color.parseColor("#FFCCCCCC"));
                            modes[j].setBackground(getResources().getDrawable(R.drawable.bg_txt_normal));
                        }
                    }
                }
            });
        }
        btnCreateRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (createRoomEdtTopic.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "聊天话题不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (createRoomEdtChannelName.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "频道名不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(MainActivity.this, ChatRoomActivity.class);
                intent.putExtra(AppConstants.ACTION_KEY_CHANNEL_NAME, createRoomEdtChannelName.getText().toString());
                intent.putExtra(AppConstants.ACTION_KEY_CHAT_TOPIC, createRoomEdtTopic.getText().toString());
                intent.putExtra(AppConstants.ACTION_KEY_CLIENT_ROLE, AppConstants.CLIENT_ROLE_OWNER);
                MainActivity.this.startActivity(intent);

                alertDialog.dismiss();
            }
        });
    }

    @OnClick(R.id.btn_join_room)
    public void onBtnJoinRoomClicked() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.setContentView(R.layout.dialog_join_room_0);
        // 这两句话是为了弹出软键盘
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        /*TextView txtSelectorTitle = alertDialog.findViewById(R.id.txt_selector_title);
        TextView txtSelectorDescription = alertDialog.findViewById(R.id.txt_selector_description);
        RecyclerView recyclerSelector = alertDialog.findViewById(R.id.recycler_selector);
        txtSelectorTitle.setText("请选择背景音乐");
        txtSelectorDescription.setText("背景音乐");
        // 初始化选择列表
        mMusicSelectorRecyclerAdapter = new MusicSelectorRecyclerAdapter(this);
        GridLayoutManager musicLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerSelector.setLayoutManager(musicLayoutManager);
        recyclerSelector.setAdapter(mMusicSelectorRecyclerAdapter);
        mMusicSelectorRecyclerAdapter.setOnItemClickListener(new MusicSelectorRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                // String filePath, bool loopback, bool replace, int cycle
                getWorkerThread().getRtcEngine().startAudioMixing("/assets/music_" + position + ".mp3", false, false, -1);
            }
        });*/

        // 让对话框显示在屏幕底部
        WindowManager manager = this.getWindowManager();
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        int width = outMetrics.widthPixels;
        int height = outMetrics.heightPixels;
        window.setLayout(width, (int) DimensionsUtils.dp2px(this, 295.33f));
        window.setGravity(Gravity.BOTTOM);
    }
}
