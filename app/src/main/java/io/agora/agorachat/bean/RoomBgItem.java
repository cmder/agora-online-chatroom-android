package io.agora.agorachat.bean;

public class RoomBgItem {
    private boolean chosen;

    public RoomBgItem(boolean chosen) {
        this.chosen = chosen;
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }
}
