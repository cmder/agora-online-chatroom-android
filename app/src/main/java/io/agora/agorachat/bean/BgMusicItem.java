package io.agora.agorachat.bean;

public class BgMusicItem {

    private String name;
    private String size;
    private String author;
    private String path;
    private int state;// 0代表播放，1代表暂停

    public BgMusicItem(String name, String size, String author, String path, int state) {
        this.name = name;
        this.size = size;
        this.author = author;
        this.path = path;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
