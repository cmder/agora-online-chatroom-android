package io.agora.agorachat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.agora.agorachat.R;
import io.agora.agorachat.bean.BgMusicItem;

public class MusicSelectorRecyclerAdapter extends RecyclerView.Adapter<MusicSelectorRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;

    private List<BgMusicItem> mBgMusicItems;

    private OnItemClickListener mOnItemClickListener;

    public MusicSelectorRecyclerAdapter(Context context, List<BgMusicItem> bgMusicItems) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mBgMusicItems = bgMusicItems;
    }

    @Override
    public MusicSelectorRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.bg_music_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        BgMusicItem item = mBgMusicItems.get(position);
        holder.name.setText(item.getName());
        holder.desc.setText(item.getSize() + "  " + item.getAuthor());
        if (item.getState() == 0) {
            holder.state.setImageResource(R.mipmap.music_play);
        } else if (item.getState() == 1) {
            holder.state.setImageResource(R.mipmap.music_pause);
        }

        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mBgMusicItems.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView desc;
        private ImageView state;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.bg_music_txt_name);
            desc = view.findViewById(R.id.bg_music_txt_desc);
            state = view.findViewById(R.id.bg_music_img_state);
        }
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }

}
