package io.agora.agorachat.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.agora.agorachat.R;
import io.agora.agorachat.bean.ChangeVoiceItem;

public class ChangeVoiceRecyclerAdapter extends RecyclerView.Adapter<ChangeVoiceRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;

    private List<ChangeVoiceItem> mChangeVoiceItems;

    private OnItemClickListener mOnItemClickListener;

    public ChangeVoiceRecyclerAdapter(Context context, List<ChangeVoiceItem> changeVoiceItems) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mChangeVoiceItems = changeVoiceItems;
    }

    @Override
    public ChangeVoiceRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.change_voice_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        ChangeVoiceItem item = mChangeVoiceItems.get(position);
        holder.option.setText(item.getTitle());
        if (item.isSelected()) {
            holder.option.setTextColor(Color.parseColor("#09BDF4"));
            holder.option.setBackgroundResource(R.drawable.bg_txt_selected);
        } else {
            holder.option.setTextColor(Color.parseColor("#CCCCCC"));
            holder.option.setBackgroundResource(R.drawable.bg_txt_normal);
        }

        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mChangeVoiceItems.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void setChosen(int position) {
        for (int i = 0; i < mChangeVoiceItems.size(); i++) {
            if (i == position) {
                mChangeVoiceItems.get(i).setSelected(true);
            } else {
                mChangeVoiceItems.get(i).setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView option;

        public ViewHolder(View view) {
            super(view);
            option = view.findViewById(R.id.change_voice_txt_option);
        }
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }

}
