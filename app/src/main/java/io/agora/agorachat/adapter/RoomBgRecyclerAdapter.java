package io.agora.agorachat.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import io.agora.agorachat.R;
import io.agora.agorachat.bean.RoomBgItem;
import io.agora.agorachat.utils.BitmapUtils;

public class RoomBgRecyclerAdapter extends RecyclerView.Adapter<RoomBgRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;

    private List<RoomBgItem> mDatas;

    private OnItemClickListener mOnItemClickListener;

    public RoomBgRecyclerAdapter(Context context, List<RoomBgItem> datas) {
        this.mContext = context;
        this.mDatas = datas;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public RoomBgRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.room_bg_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        int id = mContext.getResources().getIdentifier("io.agora.agorachat:mipmap/bg_room_" + position, null, null);
        holder.img.setImageBitmap(BitmapUtils.readBitMap(mContext, id));
        if (mDatas.get(position).isChosen()) {
            holder.selector.setVisibility(View.VISIBLE);
        } else {
            holder.selector.setVisibility(View.GONE);
        }
        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void setChosen(int position) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (i == position) {
                mDatas.get(i).setChosen(true);
            } else {
                mDatas.get(i).setChosen(false);
            }
        }
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView cv;
        public ImageView img;
        public RelativeLayout selector;

        public ViewHolder(View view) {
            super(view);
            cv = view.findViewById(R.id.room_bg_cv);
            img = view.findViewById(R.id.room_bg_img);
            selector = view.findViewById(R.id.room_bg_selector);
        }
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }

}
